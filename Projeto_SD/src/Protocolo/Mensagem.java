package Protocolo;

import Dominio.Jogador;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class Mensagem implements Serializable {

    int tarefa;//Comando que define o que o usuario deseja fazer(0 - deletar, 1 - inserir, 2 - lista usuarios)
    Jogador jogador;//parametro usado que for iserir deletar acusar ou matar jogador
    ArrayList<Jogador> listaJogadores;//lista contendo os jogadores 
    int comando;//suspect, kill(usado pelo assassino e o detetive) 
    boolean status;//estado do jogador no jogo 
    
    
    public Mensagem() {
    }

    public Mensagem(int tarefa, Jogador jogador, ArrayList<Jogador> listaJogadores, int comando, boolean status) {
        this.tarefa = tarefa;
        this.jogador = jogador;
        this.listaJogadores = listaJogadores;
        this.comando = comando;
        this.status = status;
    }

    public int getTarefa() {
        return tarefa;
    }

    public void setTarefa(int tarefa) {
        this.tarefa = tarefa;
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public ArrayList<Jogador> getListaJogadores() {
        return listaJogadores;
    }

    public void setListaJogadores(ArrayList<Jogador> listaJogadores) {
        this.listaJogadores = listaJogadores;
    }

    public int getComando() {
        return comando;
    }

    public void setComando(int comando) {
        this.comando = comando;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Mensagem{" + "tarefa=" + tarefa + ", jogador=" + jogador + ", listaJogadores=" + listaJogadores + ", comando=" + comando + ", status=" + status + '}';
    }

}
