package File;

import Dominio.Informations;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class FilePort {

    private static String portNumber;
    private static String hostName;
    private static String playerName;
    private static final File file = new File("fileNumberPort.csv");

    public static void writeInFile(ArrayList<Informations> listInformations) throws IOException {  //passa da classe produtos para o arquivo lista de produtos 

        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);

        FileWriter fwTxt = new FileWriter(file);
        PrintWriter recordFile = new PrintWriter(file);

        for (int i = 0; i < listInformations.size(); i++) {
            recordFile.printf("%s%n", listInformations.get(i));
            bw.write(listInformations.get(i).toString());
        }

        bw.close();
        fw.close();
        recordFile.close();
        System.out.println("gravado com sucesso!!!");
    }

    public static ArrayList<Informations> readFromFile() { // retorna o dados do arquivo para a classe produto 
        ArrayList<Informations> listInformations = new ArrayList<>();

        try {

            FileReader fr = new FileReader(file);
            BufferedReader readFile = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));

            String line = readFile.readLine();
            System.out.println("file: " + line);
            int j = 0;

            while (line != null) {

                String[] info = line.split(";");
                playerName = info[0];
                hostName = info[1];
                portNumber = info[2];

                Informations informations = new Informations(playerName, hostName, portNumber);

                listInformations.add(informations);
                line = readFile.readLine();
            }
            fr.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.", e.getMessage());
        }
        return listInformations;
    }
}
