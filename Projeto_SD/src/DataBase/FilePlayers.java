/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBase;

import Dominio.Jogador;
import java.util.ArrayList;

/**
 *
 * @author thales
 */
public class FilePlayers {

    ArrayList<Jogador> listPlayers = new ArrayList<>();
    ArrayList<Jogador> listPlayers2test = new ArrayList<>();

    public boolean addPlayer(Jogador player) {
        listPlayers.add(player);
        System.out.println(listPlayers);
        return true;
    }

    public boolean removePlayer() {
        
        return true;
    }

    public int getSizeList() {
        return listPlayers.size();
    }

    public boolean addlistplayers2test() {
        Jogador p1, p2, p3;

        p1 = new Jogador(1, "bob", "Ilheus", 0, true, "localhost", 5555);
        p2 = new Jogador(1, "albert", "Ilheus", 0, true, "localhost", 5555);
        p3 = new Jogador(1, "thales", "Ilheus", 0, true, "localhost", 5555);
        listPlayers2test.add(p1);
        listPlayers2test.add(p2);
        listPlayers2test.add(p3);
         
        return true;
    }

    public ArrayList<Jogador> getListPlayers(){
        return listPlayers;
    }
    
    public boolean compareLists() {
        int i = 0;
        int size = listPlayers2test.size();

        for (Jogador p1 : listPlayers) {
            for (Jogador p2 : listPlayers2test) {
                if (p1.getNome().equalsIgnoreCase(p2.getNome())) {
                    i++;
                }
            }
        }
        if (i == size) {
            return true;
        } else {
            return false;
        }
    }

}
