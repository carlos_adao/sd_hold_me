package Dominio;

/**
 *
 * @author carlos
 */
public class Informations {
    String playerName;
    String hostName;
    String numberPort;

    public Informations(String playerName, String hostName, String numberPort) {
        this.playerName = playerName;
        this.hostName = hostName;
        this.numberPort = numberPort;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getNumberPort() {
        return numberPort;
    }

    public void setNumberPort(String numberPort) {
        this.numberPort = numberPort;
    }

    @Override
    public String toString() {
        return playerName + ";" + hostName + ";" + numberPort + "\n";
    }
    
    
}
