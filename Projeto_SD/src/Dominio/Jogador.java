/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

import java.io.Serializable;

/**
 *
 * @author carlos
 */
public class Jogador implements Serializable {

    int id; //guarda o indentificador do jogado 
    String nome;
    String city;
    int tipo;
    boolean estado;//variavel usada para saber se o jogador está vivo ou morto
    String hostName;
    int numberPort;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getNumberPort() {
        return numberPort;
    }

    public void setNumberPort(int numberPort) {
        this.numberPort = numberPort;
    }
    
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Jogador(int id, String nome, String city, int tipo, boolean estado) {
        this.id = id;
        this.nome = nome;
        this.city = city;
        this.tipo = tipo;
        this.estado = estado;
    }

    public Jogador(int id, String nome, int tipo, boolean estado) {
        this.id = id;
        this.nome = nome;
        this.tipo = tipo;
        this.estado = estado;
    }

    public Jogador() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Jogador{" + "id=" + id + ", nome=" + nome + ", city=" + city + ", tipo=" + tipo + ", estado=" + estado + ", hostName=" + hostName + ", numberPort=" + numberPort + '}';
    }

    public Jogador(int id, String nome, String city, int tipo, boolean estado, String hostName, int numberPort) {
        this.id = id;
        this.nome = nome;
        this.city = city;
        this.tipo = tipo;
        this.estado = estado;
        this.hostName = hostName;
        this.numberPort = numberPort;
    }

  

}
