/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Dominio.Jogador;
import Inteface.PanelBody;
import Inteface.PanelFooter;
import Protocolo.Mensagem;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class ControleCliente {

    Cliente cliente = new Cliente();
    //metodo usado para instaciar um jogador 


    public Jogador instanciarJogador(String nome) {

        Jogador jogador = new Jogador();
        jogador.setNome(nome);

        return jogador;
    }

    //exibe os jogadores que estão conectados ao jogo
    public void exibeListaJogadores(Mensagem msg) {
        PanelFooter.setLabelInformation("Seja bem vindo");
        
        ArrayList<Jogador> listaJogadores = msg.getListaJogadores();

        for (Jogador jogador : listaJogadores) {
            System.out.println("Cod:" + jogador.getId());
            System.out.println("Nome:" + jogador.getNome());
            System.out.println("Tipo:" + jogador.getTipo());
            System.out.println("Estado:" + jogador.isEstado());
        }
    }
    //adiciona player ao painel body
    public void addPlayer(Jogador player){
         
        PanelBody.addPlayerInPanelbody(player);
    
    }
    

    //emcapsulamento para inserir um jogador recebe como parametro o jogador que sera iserido no jogo 
    public Mensagem encapsulaMgsInserir(Jogador jogador) {
        Mensagem msg = new Mensagem();
        msg.setTarefa(1);// a tarefa (1 - INSERIR JOGADOR)
        msg.setJogador(jogador);
        return msg;
    }

    //emcapsulamento para inserir um jogador recebe como parametro o jogador que sera iserido no jogo 
    public Mensagem encapsulaMsgReturnListJogadores() {
        Mensagem msg = new Mensagem();
        msg.setTarefa(3);//  (1 - LISTA DE JOGADORES)
        return msg;
    }

    public void sendMsg(Mensagem msg) {
        cliente.send(msg);
    }

}
