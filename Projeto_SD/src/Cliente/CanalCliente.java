/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Protocolo.Mensagem;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class CanalCliente implements Runnable {

    private ObjectInputStream input;
    private final ControleCliente ctrlClient = new ControleCliente();

    public CanalCliente(Socket socket) {
       
        try {
            this.input = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {

        }
    }

    @Override
    public void run() {
        Mensagem msg;
        ArrayList<String[]> produtos = null;
        try {
            while ((msg = (Mensagem) input.readObject()) != null) {
                realizaEventos(msg);
            }
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }
    }

    public void realizaEventos(Mensagem msg) {

        switch (msg.getTarefa()) {
            case 0:
                //inserir jogador no painel
                System.out.println("Lista de jogadores");
               
                ctrlClient.addPlayer(msg.getJogador());
                break;
            case 1:
                //aviso
                System.out.println("Aviso\n");
     
                break;
           
            default:
                System.out.println("Este não é um comando válido!");
        }

    }
}
