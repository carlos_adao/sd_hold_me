/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Protocolo.Mensagem;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author carlos
 */
public class Cliente {

    private Socket socket;
    private ObjectOutputStream output;

    public void connect(String host, int port) {
        new Thread(new CanalCliente(getAddress(host, port))).start();
    }

    public Socket getAddress(String host, int port) {
        
        try {
            this.socket = new Socket(host, port);
            this.output = new ObjectOutputStream(socket.getOutputStream());
        } catch (UnknownHostException ex) {

        } catch (IOException ex) {
        }

        return socket;
    }

    public void send(Mensagem mensagem) {
        try {
            output.writeObject(mensagem);

        } catch (IOException ex) {

        }
    }
}
