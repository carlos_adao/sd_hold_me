/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inteface;

import Cliente.Cliente;
import Dominio.Jogador;
import Protocolo.Mensagem;
import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author carlos
 */
public final class PanelMain extends JPanel { 
    /*firsh panel*/
    private Cliente client;
    PanelTop panelTop;
    PanelBody panelBody;
    PanelFooter panelFooter;
    
    public PanelMain(int port, String host, String playerName, String playerCity) {
        this.setLayout(null);
        this.setSize(400, 400); // larg, alt
        this.setLocation(0, 0);
        this.setBackground(Color.black);
        
        panelTop = new PanelTop();
        panelBody = new PanelBody();
        panelFooter = new PanelFooter();
        this.add(panelTop);
        this.add(panelBody);
        this.add(panelFooter);
        
        conectWithServer(port, host,playerName, playerCity);

    }

    public void conectWithServer(int numberPort, String hostName, String playerName, String playerCity){
        
        client = new Cliente();
        client.connect(hostName, numberPort);
        
        Jogador player = new Jogador();
        player.setNome(playerName);
        player.setCity(playerCity);
        player.setEstado(true);
        player.setHostName(hostName);
        player.setNumberPort(numberPort);
        
        
        Mensagem msg = new Mensagem(); 
        msg.setComando(0);
        msg.setJogador(player);
              
        
        client.send(msg);
        
        
    }
}
