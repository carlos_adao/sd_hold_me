/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Inteface;

import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author carlos
 */
public class PanelFooter extends JPanel {
    /*the panel top of page*/
    static JLabel Label_information;
    
    public PanelFooter() {
        this.setLayout(null);
        this.setSize(400, 20); // larg, alt
        this.setLocation(0, 380);
  
        addComponents();
    }
    
    public void addComponents() {

        /*componente grafico jlabel*/
        Label_information = new JLabel();
        Label_information.setBounds(new Rectangle(168, 135, 400, 30));
        Label_information.setText("Victim");
        Label_information.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 12));
        Label_information.setLocation(140, 0);//
        add(Label_information);

    }
    public static void setLabelInformation(String inf){
      Label_information.setText(inf);
    }
    
}
