/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inteface.Alerts;

import Inteface.FrameMain;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import projeto_sd.StartGame;
import projeto_sd.StartGame.Comunication;

/**
 *
 * @author carlos
 */
public class AlertStartServiceFrame extends JFrame {

    private final int largura;
    private final int altura;
    private final Container container;
    private AlertStartServicePanel alertStartServicePanel;
    private AlertStartNamePlayer alertStartNamePlayer;
    FrameMain frameMain;
    private StartGame startGame;

    public AlertStartServiceFrame(Comunication con) {

        alertStartServicePanel = new AlertStartServicePanel(this);
        alertStartNamePlayer = new AlertStartNamePlayer();
        this.startGame = startGame;
        container = getContentPane();
        this.setTitle("HOLD-ME");
        this.setLayout(null);
        this.largura = 280;
        this.altura = 280;
        this.setSize(largura, altura);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize(); // Captura a Dimentao Atual da Tela do PC
        this.setLocation((TamTela.width - largura) / 2, (TamTela.height - altura) / 2);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // DEVE FICA NAR JANELA PRINCIPAL
        this.add(alertStartServicePanel);
        this.add(alertStartNamePlayer);
        alertStartServicePanel.setVisible(true);
        alertStartNamePlayer.setVisible(false);
    }

    public void invisiblePanelAlertStartServicePanel() {
        alertStartServicePanel.setVisible(false);
        alertStartNamePlayer.setVisible(true);
    }

    public final class AlertStartServicePanel extends JPanel {

        JLabel Label_numberPort;
        JTextField Text_numberPort;
        JLabel Label_hostIp;
        JTextField Text_HostIp;
        JButton Button_Conectar, Button_Cancelar;
        private String number_port_service;
        private String ip_host;
        AlertStartServiceFrame alertStartServiceFrame;

        public AlertStartServicePanel(AlertStartServiceFrame alertStartServiceFrame) {
            this.setLayout(null);
            this.setSize(280, 280); // larg, alt
            this.setLocation(0, 0);
            instanceObjectsGrafics();
            this.alertStartServiceFrame = alertStartServiceFrame;
        }

        public void instanceObjectsGrafics() {

            Label_hostIp = new JLabel();
            Label_hostIp.setBounds(new Rectangle(168, 135, 400, 30));
            Label_hostIp.setText("Enter the end. Server IP");
            Label_hostIp.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
            Label_hostIp.setLocation(10, 10);//
            add(Label_hostIp);

            Text_HostIp = new JTextField();
            Text_HostIp.setBounds(new Rectangle(200, 135, 120, 17));
            Text_HostIp.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Text_HostIp.setSize(250, 22);
            Text_HostIp.setText("localhost");
            Text_HostIp.setLocation(10, 40);
            add(Text_HostIp);

            Label_numberPort = new JLabel();
            Label_numberPort.setBounds(new Rectangle(168, 135, 400, 30));
            Label_numberPort.setText("Enter the port number");
            Label_numberPort.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
            Label_numberPort.setLocation(10, 80);//
            add(Label_numberPort);

            Text_numberPort = new JTextField();
            Text_numberPort.setBounds(new Rectangle(200, 135, 120, 17));
            Text_numberPort.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Text_numberPort.setSize(250, 22);
            Text_numberPort.setLocation(10, 110);
            Text_numberPort.setText("5555");
            add(Text_numberPort);

            Button_Conectar = new JButton();
            Button_Conectar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Button_Conectar.setSize(110, 20);
            Button_Conectar.setLocation(10, 160);
            Button_Conectar.setText("Next");
            Button_Conectar.addActionListener(new EnventAlertStartServicePanel());
            add(Button_Conectar);

            Button_Cancelar = new JButton();
            Button_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Button_Cancelar.setSize(110, 20);
            Button_Cancelar.setLocation(150, 160);
            Button_Cancelar.setText("Cancel");
            Button_Cancelar.addActionListener(new EnventAlertStartServicePanel());
            add(Button_Cancelar);

        }

        public String getIp_host() {

            ip_host = Text_HostIp.getText();
            if (!ip_host.isEmpty()) {

                return ip_host;

            }
            return null;
        }

        public int getnumber_port_service() {

            number_port_service = Text_numberPort.getText();
            if (!number_port_service.isEmpty()) {

                return Integer.parseInt(number_port_service);

            }
            return 0;
        }

        public class EnventAlertStartServicePanel implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == Button_Conectar) {
                    String host = getIp_host();
                    int port = getnumber_port_service();
                    alertStartServiceFrame.alertStartNamePlayer.setHost(host);
                    alertStartServiceFrame.alertStartNamePlayer.setPort(port);
                    alertStartServiceFrame.invisiblePanelAlertStartServicePanel();
                    
                } else if (e.getSource() == Button_Conectar) {
                    Text_numberPort.setText("");
                    Text_numberPort.setText("");
                }
            }

        }
    }

    public final class AlertStartNamePlayer extends JPanel {

        JLabel Label_name_player;
        JTextField Text_name_player;
        JLabel Label_city_name;
        JTextField Text_city_name;
        JButton Button_Conectar, Button_Cancelar;
        private String namePlayer;
        private String nameCity;
        private int port;
        private String host;

        public AlertStartNamePlayer() {
            this.setLayout(null);
            this.setSize(280, 280); // larg, alt
            this.setLocation(0, 0);
            instanceObjectsGrafics();
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public void instanceObjectsGrafics() {
         
            Label_name_player = new JLabel();
            Label_name_player.setBounds(new Rectangle(168, 135, 400, 30));
            Label_name_player.setText("Enter your Player Name");
            Label_name_player.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
            Label_name_player.setLocation(10, 10);//
            add(Label_name_player);

            Text_name_player = new JTextField();
            Text_name_player.setBounds(new Rectangle(200, 135, 120, 17));
            Text_name_player.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Text_name_player.setSize(250, 22);
            Text_name_player.setLocation(10, 40);
            add(Text_name_player);

            Label_city_name = new JLabel();
            Label_city_name.setBounds(new Rectangle(168, 135, 400, 30));
            Label_city_name.setText("Enter your city name");
            Label_city_name.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 18));
            Label_city_name.setLocation(10, 80);//
            add(Label_city_name);

            Text_city_name = new JTextField();
            Text_city_name.setBounds(new Rectangle(200, 135, 120, 17));
            Text_city_name.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Text_city_name.setSize(250, 22);
            Text_city_name.setLocation(10, 110);
            add(Text_city_name);

            Button_Conectar = new JButton();
            Button_Conectar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Button_Conectar.setSize(110, 20);
            Button_Conectar.setLocation(10, 160);
            Button_Conectar.setText("Conect");
            Button_Conectar.addActionListener(new EnventButon());
            add(Button_Conectar);

            Button_Cancelar = new JButton();
            Button_Cancelar.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 16));
            Button_Cancelar.setSize(110, 20);
            Button_Cancelar.setLocation(150, 160);
            Button_Cancelar.setText("Cancel");
            Button_Cancelar.addActionListener(new EnventButon());
            add(Button_Cancelar);

        }

        public String getText_name_player() {

            namePlayer = Text_name_player.getText();
            if (!namePlayer.isEmpty()) {
                return namePlayer;
            } else {
                JOptionPane.showMessageDialog(null, "Insert name Player");
                Text_name_player.requestFocus();
            }
            return null;
        }

        public String getnumber_port_service() {

            nameCity = Text_city_name.getText();
            if (!nameCity.isEmpty()) {

                return nameCity;

            } else {
                JOptionPane.showMessageDialog(null, "Insert name City");
                Text_city_name.requestFocus();
            }
            return null;
        }

        public class EnventButon implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == Button_Conectar) {
                    String host = getHost();
                    String playerName = Text_name_player.getText();
                    String playerCity = Text_city_name.getText();
                    int port = getPort();
                   
                    frameMain = new FrameMain(host, port, playerName, playerCity);
                    AlertStartServiceFrame.this.dispose();

                } else if (e.getSource() == Button_Conectar) {
                    Text_city_name.setText("");
                    Text_name_player.setText("");
                }
            }

        }
    }

}
