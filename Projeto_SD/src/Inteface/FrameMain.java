/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inteface;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author carlos
 */
public class FrameMain extends JFrame {

    private final Container container;
    private final int largura;
    private final int altura;
    private final PanelMain panelMain;
    String host;
    int port;

    public FrameMain(String host, int port, String playerName, String playerCity) {
        this.panelMain = new PanelMain(port, host, playerName, playerCity);
        this.port = port;
        this.host = host;
        container = getContentPane();
        this.setTitle("HOLD-ME");
        this.setLayout(null);
        this.largura = 400;
        this.altura = 430;
        this.setSize(largura, altura);
        Dimension TamTela = Toolkit.getDefaultToolkit().getScreenSize(); // Captura a Dimentao Atual da Tela do PC
        this.setLocation((TamTela.width - largura) / 2, (TamTela.height - altura) / 2);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // DEVE FICA NAR JANELA PRINCIPAL
        this.container.add(panelMain);
    }
}
