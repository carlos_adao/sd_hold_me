/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inteface;

import Dominio.Jogador;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public final class PanelBody extends JPanel {
    /*the panel boby of page*/

    static JLabel Image_tip_player = new JLabel();
    static JLabel Label_tipo_jogador;
    static JLabel Label_name_player;
    static JLabel Label_status_player;
    PanelListOfPlayers panelListOfPlayers;
    PanelComandos panelComandos;

    static ImageIcon det;
    static ImageIcon kil;
    static ImageIcon vic;

    static int tipPlayer;

    public PanelBody() {
        panelListOfPlayers = new PanelListOfPlayers(this);
        panelComandos = new PanelComandos(this);
        this.setLayout(null);
        this.setSize(400, 300); // larg, alt
        this.setLocation(0, 80);
        this.add(panelListOfPlayers);
        this.add(panelComandos);
        this.setBackground(Color.white);
        addComponents();
        panelListPlayersVisible();

        vic = new ImageIcon(getClass().getResource("victim.png"));
        det = new ImageIcon(getClass().getResource("detective.png"));
        kil = new ImageIcon(getClass().getResource("killer.png"));

    }
    /*metodo usado para deixar o painel comando falso e o lista verdadeiro*/

    public void panelListPlayersVisible() {
        panelComandos.setVisible(false);
        panelListOfPlayers.setVisible(true);
    }

    /*metodo usado para deixar o painel comando verdadeiro e o lista false*/
    public void panelComandosVisible() {

        panelComandos.setVisible(true);
        panelListOfPlayers.setVisible(false);

    }

    public void addComponents() {

        Image_tip_player.setIcon(kil);
        Image_tip_player.setSize(100, 100);
        Image_tip_player.setLocation(30, 10);
        add(Image_tip_player);

        /*componente grafico jlabel*/
        Label_tipo_jogador = new JLabel();
        Label_tipo_jogador.setBounds(new Rectangle(168, 135, 400, 30));
        Label_tipo_jogador.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
        Label_tipo_jogador.setLocation(45, 110);//
        add(Label_tipo_jogador);

        Label_name_player = new JLabel();
        Label_name_player.setBounds(new Rectangle(168, 135, 400, 30));
        Label_name_player.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 25));
        Label_name_player.setLocation(35, 150);//
        add(Label_name_player);

        Label_status_player = new JLabel();
        Label_status_player.setBounds(new Rectangle(168, 135, 400, 100));
        Label_status_player.setText(".");
        Label_status_player.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 90));
        Label_status_player.setLocation(0, 92);//
        Label_status_player.setForeground(Color.GREEN);
        add(Label_status_player);

    }

    public static void addPlayerInPanelbody(Jogador player) {
        setLabel_name_player(player.getNome());
        setLabel_tipo_jogador(player.getTipo());
        setImageTipPlayer(player.getTipo());
        setLabel_status_player(player.isEstado());
        setTipPlayer(player.getTipo());
    }

    public static void setLabel_name_player(String name) {

        Label_name_player.setText(name);
    }

    public static void setLabel_tipo_jogador(int tipPlayer) {

        if (tipPlayer == 0) {
            Label_tipo_jogador.setText("Victin");
        } else if (tipPlayer == 1) {
            Label_tipo_jogador.setText("Detectve");
        } else {
            Label_tipo_jogador.setText("killer");
        }
    }

    public static void setTipPlayer(int tipPlayer) {
        PanelBody.tipPlayer = tipPlayer;
    }

    public static int getTipPlayer() {
        return tipPlayer;
    }

    public static void setImageTipPlayer(int tipPlayer) {

        if (tipPlayer == 0) {
            Image_tip_player.setIcon(vic);
        } else if (tipPlayer == 1) {
            Image_tip_player.setIcon(det);
        } else {
            Image_tip_player.setIcon(kil);
        }
    }

    public static void setLabel_status_player(boolean status) {

        if (status) {
            Label_status_player.setForeground(Color.GREEN);
        } else {
            Label_status_player.setForeground(Color.RED);
        }
    }

    public final class PanelListOfPlayers extends JPanel {

        JLabel Label_titulo;
        JList list;
        JScrollPane pane;
        PanelBody panelBody;
        String namePlayerSelected;
        int tipPlayer;

        public PanelListOfPlayers(PanelBody panelBody) {
            this.panelBody = panelBody;
            this.setLayout(null);
            this.setSize(200, 330); // larg, alt
            this.setLocation(200, 0);

            addComponents();

        }

        public void addComponents() {

            /*componente grafico jlabel*/
            Label_titulo = new JLabel();
            Label_titulo.setBounds(new Rectangle(168, 135, 400, 30));
            Label_titulo.setText("Players on");
            Label_titulo.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
            Label_titulo.setLocation(24, 10);//
            add(Label_titulo);

            /*componente grafico list view*/
            String[] options = {"Carlos Adão", "Eliane Matos ", "Eva Maria", "Ana Paula"};
            list = new JList(options);
            list.setVisibleRowCount(1);
            list.addMouseListener(new ClickNaListaJogadores());
            //list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
            pane = new JScrollPane(list);
            pane.setLocation(25, 50);
            pane.setSize(150, 200);/*Largara e altura*/

            add(pane);

        }

        public void visiblePanelComands(String nameSelectedPlayer, int tipPlayer) {

            ImageIcon knife = new ImageIcon(getClass().getResource("knife.png"));
            ImageIcon lupe = new ImageIcon(getClass().getResource("lupa.png"));
            if (tipPlayer == 1) {
                panelBody.panelComandosVisible();
                panelBody.panelComandos.addComponents(nameSelectedPlayer, lupe, tipPlayer);
            } else if (tipPlayer == 2) {
                panelBody.panelComandosVisible();
                panelBody.panelComandos.addComponents(nameSelectedPlayer, knife, tipPlayer);
            } else {

            }
        }

        public class ClickNaListaJogadores implements MouseListener {

            @Override
            public void mouseClicked(MouseEvent e) {

                int index = list.getSelectedIndex();
                String s = (String) list.getSelectedValue();

                if (panelBody.tipPlayer == 1) {//detective
                    visiblePanelComands(s, 1);
                } else if (panelBody.tipPlayer == 2) {//killer
                    visiblePanelComands(s, 2);
                } else {//victin
                }

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        }

    }

    public final class PanelComandos extends JPanel {

        PanelBody panelBody;
        JLabel imagem = new JLabel();
        JLabel imgFuncution = new JLabel();
        JLabel Label_nome_jogador = new JLabel();

        public PanelComandos(PanelBody panelBody) {
            this.panelBody = panelBody;
            this.setLayout(null);
            this.setSize(200, 330); // larg, alt
            this.setLocation(200, 0);

        }

        public void setLabel_nome_jogador(String playerSelected) {

            Label_nome_jogador.setText(playerSelected);
        }

        public void addComponents(String namePlayer, ImageIcon icon, int tipPlayer) {

            /*componente grafico jlabel*/
            Label_nome_jogador.setBounds(new Rectangle(168, 135, 400, 30));
            Label_nome_jogador.setFont(new Font("Bodoni Bd BT", Font.PLAIN, 20));
            Label_nome_jogador.setLocation(24, 80);//
            Label_nome_jogador.setText(namePlayer);
            add(Label_nome_jogador);

            ImageIcon setinha = new ImageIcon(getClass().getResource("setinha.png"));
            imagem.setIcon(setinha);
            imagem.setSize(40, 40);
            imagem.setLocation(10, 250);
            imagem.addMouseListener(new clickMouse());
            add(imagem);

            imgFuncution.setIcon(icon);
            imgFuncution.setSize(80, 80);
            imgFuncution.setLocation(10, 30);
            add(imgFuncution);

            if (tipPlayer == 1) {

                this.setBackground(Color.BLUE);
                imgFuncution.setSize(100, 120);
                imgFuncution.setLocation(80, 25);
                Label_nome_jogador.setForeground(Color.WHITE);

            } else if (tipPlayer == 2) {
                this.setBackground(Color.RED);
            } else {
                this.setBackground(Color.WHITE);
            }
        }

        public class clickMouse implements MouseListener {

            @Override
            public void mouseClicked(MouseEvent e) {

                panelBody.panelListPlayersVisible();

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        }
    }

}
