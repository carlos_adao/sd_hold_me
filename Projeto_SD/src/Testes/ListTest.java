/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Testes;

import java.awt.*;
import javax.swing.*;
public class ListTest {
    
    public static void main(String[] args)
    {
        String[] options = { "Carlos", "Thales", "Magno" , "Carlos", "Thales", "Magno" , "Carlos", "Thales", "Magno",  "Thales", "Magno" , "Carlos", "Thales", "Magno"};
        JList list = new JList(options);
        list.setVisibleRowCount(1);
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(new JScrollPane(list));
        f.setSize(400,100);
        f.setLocation(200,200);
        f.setVisible(true);
    }
    
}
