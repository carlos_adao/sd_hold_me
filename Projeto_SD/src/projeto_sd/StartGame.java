/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto_sd;

import Cliente.Cliente;
import Dominio.Informations;
import File.FilePort;
import Inteface.Alerts.AlertStartServiceFrame;
import Inteface.FrameMain;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class StartGame {
    static Comunication con;
    
    public static class Comunication {

        private final AlertStartServiceFrame alertStartServiceFrame;

        public Comunication() {            
            alertStartServiceFrame = new AlertStartServiceFrame(this);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        con = new Comunication();
        /*Jogador jogador;
         Mensagem msg = new Mensagem();
         ControleCliente controleCliente = new ControleCliente();

         int i = 0;
         //while (i < 2) {
         Thread t = new Thread(() -> {
         controleCliente.conect();
         controleCliente.sendMsg(msg);
         });
         t.start();
         //  i++;
         //}
         msg.setTarefa(3);
       
         /*usado para gravar informações no arquivo*/
        //takeInformations();
        //sendsInformationsForRecordFile(takeInformations());
        /*
         controleCliente.conect();//conecta com o servidor
         for (int i = 0; i < 10; i++) {
         jogador = controleCliente.instanciarJogador("carlos "+i);//passa o nome para um objeto jogador e o retorna
         msg = controleCliente.encapsulaMgsInserir(jogador);//encapsula a mensagem no protocolo para enviar pelo socket
         controleCliente.enviaMsg(msg);//inserir um novo jogador
         }
         msg = controleCliente.encapsulaMsgReturnListJogadores();//encapsula a mensagem no protocolo para enviar pelo socket
         controleCliente.enviaMsg(msg);//retornar a lista com os jogadores
         */
        
    }

    public static String pegaDadosInsere() {
        Scanner ler = new Scanner(System.in);
        String entrada;

        System.out.println("Digite o nome do jogador: ");
        entrada = ler.nextLine();

        return entrada;

    }

    public static ArrayList<Informations> takeInformations() {
        Informations info;
        ArrayList<Informations> listInformations = new ArrayList<>();

        info = new Informations("Carlos", "localhost", "1234");
        listInformations.add(info);

        info = new Informations("Thales", "localhost", "1235");
        listInformations.add(info);

        info = new Informations("Lucas", "localhost", "1236");
        listInformations.add(info);

        return listInformations;
    }

    public static void sendsInformationsForRecordFile(ArrayList<Informations> listInformations) {
        try {
            FilePort.writeInFile(listInformations);
        } catch (IOException ex) {
            System.err.println("Falha ao enviar dados para gravar no arquivo\n" + ex);
        }
    }

}
