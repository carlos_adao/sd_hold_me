/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import DataBase.FilePlayers;
import Dominio.Jogador;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author carlos
 */
public class ControleServidor {

    FilePlayers fplayer;

    //instancia um jogador para colocalo na lista(adiciona mais atributos ele já vem com o nome) 
    public boolean addAtribPlayer(Jogador player) {
        player.setId(idGenerator());
        fplayer.addPlayer(player);

        if (fplayer.compareLists()) {
            sortPlayersType(fplayer.getListPlayers());

        } else {
            System.out.println("erro /=");
        }
        return false;
    }

    public ArrayList<Jogador> sortPlayersType(ArrayList<Jogador> listPlayers) {
        boolean haskiller = false, hasdetective = false;
        int detetive = 0, assassino = 0, vitima = 0;
        int max = listPlayers.size();
        int min = 0;

        detetive = numeroAleatorio(min, max);

        do {

            assassino = numeroAleatorio(min, max);

        } while (assassino == detetive);

        for (int i = 0; i < max; i++) {
            
            Jogador player =(Jogador)listPlayers.get(i);
            
            if (i == assassino) {
                
                player.setTipo(1);
                
            } else if (i == detetive) {
                
                player.setTipo(2);
                
            } else if ((i != assassino) && (i != detetive)) {
                
                player.setTipo(vitima);
            
            }
        }

        System.out.println(listPlayers);
        return listPlayers;
    }

    public int numeroAleatorio(int min, int max) {

        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public int idGenerator() {

        return fplayer.getSizeList() + 1;
    }

    public ControleServidor(FilePlayers fplayer) {
        this.fplayer = fplayer;
    }
}