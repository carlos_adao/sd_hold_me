/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import DataBase.FilePlayers;
import Dominio.Jogador;
import Protocolo.Mensagem;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class CanalServidor implements Runnable {

    private ObjectOutputStream output;
    private ObjectInputStream input;
    ControleServidor ctrlServidor;
    FilePlayers fplayer;

    public CanalServidor(Socket socket, FilePlayers fplayer) {
        this.fplayer = fplayer;
        
        ctrlServidor = new ControleServidor(fplayer);
        
       try {

            this.output = new ObjectOutputStream(socket.getOutputStream());
            this.input = new ObjectInputStream(socket.getInputStream());

            //ListaEnderecos.add(output);
        } catch (IOException ex) {
            System.out.println("cath " + ex);
        }
    }

    @Override
    public void run() {
        Mensagem mensagem;
        try {
            while ((mensagem = (Mensagem) this.input.readObject()) != null) {

                realizaEventos(mensagem);

            }
        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        }
    }

    public void realizaEventos(Mensagem msg) {
        Jogador jogador;
        Mensagem serveMsg;

        switch (msg.getTarefa()) {
            case 0:
                //adiciona jogador
                System.out.println("player starter");
                Jogador player = msg.getJogador();
                ctrlServidor.addAtribPlayer(player);
                
                /*serveMsg = new Mensagem();
                Jogador player = new Jogador();
                player.setId(1);
                player.setNome("Carlos Adão");
                player.setTipo(2);
                player.setEstado(true);
                serveMsg.setComando(0);
                serveMsg.setJogador(player);

                send(serveMsg);
                */
                break;
            case 1:
                
                break;
            case 3:
                //retornar lista de usuario no jogo
                //send(ctrlServidor.encapsulaReturnListJogadores());
                System.out.println("retornando a lista de jogadores");
                break;
            case 4:
                System.out.println("atualizar lista de jogadores");
                //atualizar lista de jogadores 
                break;
            case 5:
                System.out.println("matar jogador");
                //matar jogador 
                break;
            case 6:
                System.out.println("suspeitar de jogador");
                //suspeitar de jogador
                break;
            default:
                System.out.println("Este não é um comando válido!");
        }

    }

    public void send(Mensagem mensagem) {
        try {
            output.writeObject(mensagem);

        } catch (IOException ex) {
            System.out.println("Esception ao escrever objeto no canal: " + ex);
        }
    }
}
