package Servidor;

import DataBase.FilePlayers;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author carlos
 */
public class Servidor {

    static ServerSocket serverSocket;
    static Socket socket;
    private int tipService;

    private static FilePlayers fplayer;

    public static void main(String[] args) throws IOException, InterruptedException {
        startService(getPort());

    }

    private static void startService(int porta) throws IOException, InterruptedException {
        fplayer = new FilePlayers();
        fplayer.addlistplayers2test();
        try {
            serverSocket = new ServerSocket(porta);

            System.out.println("Servidor on!");

            while (true) {
                System.out.println("\nAguardando conexão...");
                socket = serverSocket.accept();
                System.out.println("\nConexão Estabelecida");

                System.err.println(socket.getInetAddress().getHostName());

                new Thread(new CanalServidor(socket, fplayer)).start();
            }

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex);

        }

    }

    /*Obtem numero da porta para o servidor */
    public static int getPort() {
        Scanner ler = new Scanner(System.in);
        int porta;

        System.out.println("Informe o numero da porta que irá oferecer o servico:");
        porta = ler.nextInt();

        return porta;
    }

}
